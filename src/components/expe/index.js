import React, { useState } from "react";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core/styles";
import CardSession from "../cardSession";
import Session from "../Session";



const Expe = (props) => {
  const [currentSession, setCurrentSession] = useState(0);
  const useStyles = makeStyles({
    gridContainer: {
      paddingLeft: "40px",
      paddingRight: "40px",
    },
  });
  const classes = useStyles();
  const { group } = props.userData;
  
  return currentSession === 0 ? (
    <div>
      <h2>Choisis la session du jour :</h2>
      <p>group : {group}</p>
      <br />
      <Grid
        container
        spacing={4}
        className={classes.gridContainer}
        justifyContent="center"
      >
        <Grid item xs={12} sm={6} md={4}>
          <CardSession
            disabled={false}
            setCurrentSession={setCurrentSession}
            session={'pt'}
            img={
              "https://img-4.linternaute.com/1BjXVA6uHznWA1dWbagxZ8cFusw=/1080x/smart/50fbc07372594b408a73658248e4b662/ccmcms-linternaute/11941512.jpg"
            }
            text={""}
            title={"Session 0"}
          />
        </Grid>
        <Grid item xs={12} sm={6} md={4}>
          <CardSession
            disabled={false}
            setCurrentSession={setCurrentSession}
            session={1}
            img={
              "https://format-com-cld-res.cloudinary.com/image/private/s--bWguVhLc--/c_limit,g_center,h_65535,w_550/fl_keep_iptc.progressive,q_95/v1/75901348c7942696190ba7e96ddb401f/NYTMAGKIDS.jpg?550"
            }
            text={"Les mots de l’altruisme : empathie, entraide, coopération"}
            title={"Session 1"}
          />
        </Grid>
        <Grid item xs={12} sm={6} md={4}>
          <CardSession
            setCurrentSession={setCurrentSession}
            session={2}
            img={
              "https://static01.nyt.com/images/2020/09/04/opinion/sunday/04worthen-web-1/04worthen-web-1-superJumbo.jpg?quality=90&auto=webp"
            }
            text={"Les mots de l’altruisme : empathie, entraide, coopération"}
            title={"Session 2"}
          />
        </Grid>
        <Grid item xs={12} sm={6} md={4}>
          <CardSession
            disabled={true}
            setCurrentSession={setCurrentSession}
            session={3}
            img={
              "https://static01.nyt.com/images/2019/05/06/parenting/00-parenting-rollingover/00-parenting-rollingover-superJumbo.jpg?quality=90&auto=webp"
            }
            text={"Les bébés sont-ils altruistes ? Jalons développementaux"}
            title={"Session 3"}
          />
        </Grid>
        <Grid item xs={12} sm={6} md={4}>
          <CardSession
            setCurrentSession={setCurrentSession}
            session={4}
            img={
              "https://static01.nyt.com/images/2017/08/27/books/review/0827-BKS-Palacio01/0827-BKS-Palacio01-jumbo.jpg?quality=90&auto=webp"
            }
            text={"Les bébés sont-ils altruistes ? Jalons développementaux"}
            title={"Session 4"}
          />
        </Grid>
        <Grid item xs={12} sm={6} md={4}>
          <CardSession
            setCurrentSession={setCurrentSession}
            session={5}
            img={
              "https://static01.nyt.com/images/2019/07/15/smarter-living/15sl_newsletter/00sl_overstimulation-jumbo.jpg?quality=90&auto=webp"
            }
            text={
              "Réfléchir l’altruisme : importance des mécanismes cognitifs (attention, TOM, inhibition)"
            }
            title={"Session 5"}
          />
        </Grid>
        <Grid item xs={12} sm={6} md={4}>
          <CardSession
            setCurrentSession={setCurrentSession}
            session={6}
            img={
              "https://exploringyourmind.com/wp-content/uploads/2018/02/theory-of-min.jpg"
            }
            text={
              "Réfléchir l’altruisme : importance des mécanismes cognitifs (attention, TOM, inhibition)n"
            }
            title={"Session 6"}
          />
        </Grid>
        <Grid item xs={12} sm={6} md={4}>
          <CardSession
            setCurrentSession={setCurrentSession}
            session={7}
            img={
              "https://static01.nyt.com/images/2019/04/02/smarter-living/yolb-empathy-guide-images-slide-OZ0P/yolb-empathy-guide-images-slide-OZ0P-jumbo.jpg"
            }
            text={"S’engager vers autrui : exemple du volontariat"}
            title={"Session 7"}
          />
        </Grid>
        <Grid item xs={12} sm={6} md={4}>
          <CardSession
            setCurrentSession={setCurrentSession}
            session={8}
            img={
              "https://static01.nyt.com/images/2012/05/06/business/0506SBNCAREERS/0506SBNCAREERS-jumbo.jpg?quality=90&auto=webp"
            }
            text={"S’engager vers autrui : exemple du volontariat"}
            title={"Session 8"}
          />
        </Grid>
        <Grid item xs={12} sm={6} md={4}>
          <CardSession
            setCurrentSession={setCurrentSession}
            session={9}
            img={
              "https://media.npr.org/assets/img/2019/06/10/l-hyrbyk-kindness_slide-95412184cb309ba9c3ce374dd1d2e7b6554ecd9f-s800-c85.webp"
            }
            text={
              "Altruistes ensemble : actions en classe ou dans une association"
            }
            title={"Session 9"}
          />
        </Grid>
        <Grid item xs={12} sm={6} md={4}>
          <CardSession
            setCurrentSession={setCurrentSession}
            session={10}
            img={
              "https://i1.wp.com/blog.ed.ted.com/wp-content/uploads/2018/07/ted_activistkids_revised_hires.jpg?resize=575%2C323"
            }
            text={
              "Altruistes ensemble : actions en classe ou dans une association"
            }
            title={"Session 10"}
          />
        </Grid>

        <Grid item xs={12} sm={6} md={4}>
          <CardSession
            setCurrentSession={setCurrentSession}
            session={11}
            img={
              "https://www.vwt.org.au/wp-content/uploads/2020/03/72818278473391.5ca5cd82d13b7.png"
            }
            text={
              "Altruistes au quotidien : agir pour ses voisins et sa planète dans les petits gestes"
            }
            title={"Session 11"}
          />
        </Grid>

        <Grid item xs={12} sm={6} md={4}>
          <CardSession
            setCurrentSession={setCurrentSession}
            session={12}
            img={
              "https://static01.nyt.com/newsgraphics/2020/09/22/climate-future/b5c4cdb7f9930112de84bb4cfd22101c30ff1ab0/desktop/climate_d15-future-trees.png"
            }
            text={
              "Altruistes au quotidien : agir pour ses voisins et sa planète dans les petits gestes"
            }
            title={"Session 12"}
          />
        </Grid>

        <Grid item xs={12} sm={6} md={4}>
          <CardSession
            setCurrentSession={setCurrentSession}
            session={13}
            img={
              "https://static01.nyt.com/images/2020/09/27/books/review/27PictureThis/27PictureThis-jumbo.jpg?quality=90&auto=webp"
            }
            text={"Altruisme à travers les cultures"}
            title={"Session 13"}
          />
        </Grid>

        <Grid item xs={12} sm={6} md={4}>
          <CardSession
            setCurrentSession={setCurrentSession}
            session={14}
            img={
              "https://static01.nyt.com/images/2018/03/20/science/20BRODY/20BRODY-jumbo.jpg?quality=90&auto=webp"
            }
            text={"Altruisme à travers les cultures"}
            title={"Session 14"}
          />
        </Grid>

        <Grid item xs={12} sm={6} md={4}>
          <CardSession
            setCurrentSession={setCurrentSession}
            session={15}
            img={
              "https://static01.nyt.com/images/2020/09/19/books/review/19PictureThis_Image2/19PictureThis_Image2-jumbo.jpg?quality=90&auto=webp"
            }
            text={"Et maintenant, je fais comment ? Des idées pour agir"}
            title={"Session 15"}
          />
        </Grid>

        <Grid item xs={12} sm={6} md={4}>
          <CardSession
            setCurrentSession={setCurrentSession}
            session={16}
            img={
              "https://dcxvepib5r3t6.cloudfront.net/attachments/file_s3s/5de4/df1e/0200/3e00/072c/b468/original/geoff-mcfetridge-metalmagazine-5.jpg?1575280413"
            }
            text={"Et maintenant, je fais comment ? Des idées pour agir"}
            title={"Session 16"}
          />
        </Grid>
      </Grid>
    </div>
  ) : (
    <div>
      <Session currentSession={currentSession} />
    </div>
  );
};

export default Expe;
