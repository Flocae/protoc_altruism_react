import React from "react";

const Footer = () => {
  return (
    <footer>
      <div className="footer-container">
        <a href={"/signup"}>Ajouter un utilisateur</a>
        <p>Projet réalisé dans le cadre de XXX - <a href="/legal">mentions légales</a></p>
        <p>
          Développement de l'application web :{" "}
          <a href="https:///florentcaetta.fr" target="_blank" rel="noreferrer">
             Florent Caetta
          </a>
        </p>
      </div>
    </footer>
  );
};

export default Footer;
