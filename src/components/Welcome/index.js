import React,{useState,Fragment,useContext,useEffect} from "react";
import {FirebaseContext} from '../Firebase';
import LogOut from "../logout";
import Expe from "../expe";

const Welcome = props => {

    const firebase = useContext(FirebaseContext);
    const [userSession, setUserSession] = useState(null);
    const [userData, setUserData] = useState({});

    useEffect(() => { //On utilise ce Hook pour indiquer à React que notre composant doit exécuter quelque chose après chaque affichage. = componentDidMount pour les classes
        let listener = firebase.auth.onAuthStateChanged(user=>{
            user?setUserSession(user):props.history.push('/')
        })
        if (!!userSession) {
            firebase.user(userSession.uid)
            .get()// collect info relatives à cet user
            .then(doc=>{
                if (doc && doc.exists){
                    const myData = doc.data();
                    setUserData(myData);
                    firebase.userData=myData;// Set global user variable via context
                }
            })// si réponse
            .catch(error=>{
                console.log(error);
            })// si erreur
        }
        
        return () => {
            listener()
        }
    }, [userSession, firebase, props.history])

 
    
    return userSession===null?(
        <Fragment>
            <div className="loader"></div>
            <p className="loaderText">Chargement...</p>
        </Fragment>
        
    ):(
        <div className="quiz-bg" >
        <div className="container" >
            <LogOut userData={userData}/>
            <Expe userData={userData} />
        </div>
    </div>
    )

    
}

export default Welcome