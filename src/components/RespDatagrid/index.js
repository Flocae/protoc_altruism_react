import React from "react";
import { DataGrid } from "@mui/x-data-grid";

export const RespDatagrid = () => {
  return (
    <div style={{ height: '100%', width: '100%' }}>
      <DataGrid
        rows={rows}
        columns={columns}
        hideFooter={true}
        hideFooterPagination={true}
        autoHeight={true}
        disableColumnMenu={true}
        rowCount={3}
        showCellRightBorder={true}
        autoPageSize={true}
        resizable={true}
      />
    </div>
  );
};

const columns = [
  { field: "r1", headerName: "R",  headerAlign: 'center', flex: 1, editable: true, sortable: false },
  { field: "r2", headerName: "B", headerAlign: 'center', flex: 1, editable: true, sortable: false },
  { field: "r3", headerName: "Y", headerAlign: 'center', flex: 1, editable: true, sortable: false},
];
const rows = [
  {
    id: 1,
    r1: "",
    r2: "",
    r3: "",
  },
  {
    id: 2,
    r1: "",
    r2: "",
    r3: "",
  },
  {
    id: 3,
    r1: "",
    r2: "",
    r3: "",
  },
  {
    id: 4,
    r1: "",
    r2: "",
    r3: "",
  },
  {
    id: 5,
    r1: "",
    r2: "",
    r3: "",
  },
  {
    id: 6,
    r1: "",
    r2: "",
    r3: "",
  },
  {
    id: 7,
    r1: "",
    r2: "",
    r3: "",
  },
  {
    id: 8,
    r1: "",
    r2: "",
    r3: "",
  },
  {
    id: 9,
    r1: "",
    r2: "",
    r3: "",
  },
  {
    id: 10,
    r1: "",
    r2: "",
    r3: "",
  },
  {
    id: 11,
    r1: "",
    r2: "",
    r3: "",
  },
  {
    id: 12,
    r1: "",
    r2: "",
    r3: "",
  },
  {
    id: 13,
    r1: "",
    r2: "",
    r3: "",
  },
  {
    id: 14,
    r1: "",
    r2: "",
    r3: "",
  },
  {
    id: 15,
    r1: "",
    r2: "",
    r3: "",
  },
  {
    id: 16,
    r1: "",
    r2: "",
    r3: "",
  },
  {
    id: 17,
    r1: "",
    r2: "",
    r3: "",
  },
  {
    id: 18,
    r1: "",
    r2: "",
    r3: "",
  },
  {
    id: 19,
    r1: "",
    r2: "",
    r3: "",
  },
  {
    id: 20,
    r1: "",
    r2: "",
    r3: "",
  },
];
