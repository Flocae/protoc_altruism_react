import React from "react";
import { ReactP5Wrapper } from "react-p5-wrapper";
// Variables
let sktWidth=0;
let img;
function sketch(p5) {
  p5.preload=()=>{
    img = p5.loadImage('https://res.cloudinary.com/rebelwalls/image/upload/b_black,c_fill,f_auto,fl_progressive,h_533,q_auto,w_800/v1428564288/article/R10141_image1');
  }
  p5.setup = () =>{
    p5.createCanvas(sktWidth, 400, p5.WEBGL);
    p5.background(50);
    p5.image(img,-img.width/2,-img.height/2);
  }
  p5.draw = () => {
   /* p5.background(250);
    p5.normalMaterial();
    p5.push();
    p5.rotateZ(p5.frameCount * 0.01);
    p5.rotateX(p5.frameCount * 0.01);
    p5.rotateY(p5.frameCount * 0.01);
    p5.plane(100);
    p5.pop();*/
  };
  p5.mousePressed = () => {
    console.log("hi from P5")
  };
}

function MySketch(props) {
    sktWidth=props.w;
  return <ReactP5Wrapper sketch={sketch} />;
}

export default MySketch;