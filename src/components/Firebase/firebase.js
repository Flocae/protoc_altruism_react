import app from 'firebase/compat/app';
import 'firebase/compat/auth';
import 'firebase/compat/firestore';

const config = {
  apiKey: 'AIzaSyCxzDwMtvP-34vZA6E0fsgIjKR2VuAJ8MQ',
  authDomain: 'projet-philan-gdevelop.firebaseapp.com',
  databaseURL:
    'https://projet-philan-gdevelop-default-rtdb.europe-west1.firebasedatabase.app',
  projectId: 'projet-philan-gdevelop',
  storageBucket: 'projet-philan-gdevelop.appspot.com',
  messagingSenderId: '820393694701',
  appId: '1:820393694701:web:f5e3f8822e08e5a54803e4',
  measurementId: 'G-WMGNRNL4XH',
};

class Firebase {
  constructor() {
    app.initializeApp(config);
    this.auth = app.auth();
    this.db = app.firestore();
  }

  // Inscription
  signupUser = (email, password) =>
    this.auth.createUserWithEmailAndPassword(email, password);

  // Connexion
  loginUser = (email, password) =>
    this.auth.signInWithEmailAndPassword(email, password);

  // Deconnexion
  signoutUser = () => this.auth.signOut();

  // Récupérer le mot de passe
  passwordReset = (email) => this.auth.sendPasswordResetEmail(email);

  // firestore
  user = (uid) => this.db.doc(`users/${uid}`);// données utilisateurs
  databehav = (pseudo) => this.db.doc(`databehav/${pseudo}`); // Données comportementales

  // Global variable
  userData = {};
}

export default Firebase;
