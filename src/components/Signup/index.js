import React, { useState,useContext } from "react";
import {FirebaseContext} from '../Firebase';
import { Link } from "react-router-dom";

const Signup = (props) => {
    const firebase = useContext(FirebaseContext);
    
    const data = {
        pseudo: '',
        email: '',
        group:'',
        password: '',
        confirmPassword: '',
        verifCode:''
    }
    const [loginData, setLoginData] = useState(data);
    const [error,setError]= useState('')

    const handleChange = e => {
        setLoginData({...loginData, [e.target.id]: [e.target.value] })
    }
    const handleSubmit=e=>{
        e.preventDefault();// Empeche la page de se rafraichier lors de la validation du formulaire
        const {email, password, pseudo,group} = loginData;
        firebase.signupUser(email.toString(),password.toString())
        .then(authUser =>{
            return firebase.user(authUser.user.uid).set({
                pseudo,
                email,
                group
            })
        })
        .then(() =>{
            setLoginData({...data});// Reinitialise les données
            props.history.push('/welcome');
        })
        .catch(error=>{
            setError(error);
            setLoginData({...data});// Reinitialise les données
        })
    }

    const { pseudo, email,group, password, confirmPassword,verifCode} = loginData;

    const btn = pseudo.toString() !== '' && email.toString()  !== '' && password.toString()  !== '' && confirmPassword.toString() !=='' && confirmPassword.toString()===password.toString() && verifCode.toString()==='123456'
    ? <button >Inscription</button> : <button disabled>Inscription</button>

    // Gestion des erreurs
    const errorMsg = error !=='' && <span>{error.message}</span>
    
    return (
        <div className="signUpLoginBox">
            <div className="slContainer">
                <div className="formBoxLeftSignup">
                </div>
                <div className="formBoxRight">
                    <div className="formContent">
                        {errorMsg}
                        <h2>Enregistrement d'un nouvel utilisateur</h2>
                        <p style={{color: "white", fontSize:'12px'}}> Cette espace est uniquement destiné aux administrateurs de la plateforme. Le mot de passe doit comporter au moins six caractères.</p>
                        <br/>
                        <form onSubmit={handleSubmit}>
                            <div className="inputBox">
                                <input onChange={handleChange} value={pseudo} type="text" id="pseudo" autoComplete="off" required />
                                <label htmlFor="pseudo">Pseudo</label>
                            </div>
                            <div className="inputBox">
                                <input onChange={handleChange} value={email} type="email" id="email" autoComplete="off" required />
                                <label htmlFor="email">email</label>
                            </div>
                            <div className="inputBox">
                                <input onChange={handleChange} value={group} type="text" id="group" autoComplete="off" required />
                                <label htmlFor="email">Groupe</label>
                            </div>
                            <div className="inputBox">
                                <input onChange={handleChange} value={password} type="password" id="password" autoComplete="off" required/>
                                <label htmlFor="password">Mot de passe</label>
                            </div>
                            <div className="inputBox">
                                <input onChange={handleChange} value={confirmPassword} type="password" id="confirmPassword" autoComplete="off" required />
                                <label htmlFor="confirmPassword">Confirmer le mot de passe</label>
                            </div>
                            <div className="inputBox">
                                <input onChange={handleChange} value={verifCode} type="text" id="verifCode" autoComplete="off" required />
                                <label htmlFor="verifCode">Code de vérification</label>
                            </div>
                            {btn}
                        </form>
                        <div className="linkContainer">
                            <Link className="simpleLink" to="/login"> Déjà inscrit ? Connectez-vous
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Signup