import React, { useContext } from 'react';
import { FirebaseContext } from '../Firebase';
import IconButton from '@material-ui/core/IconButton';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import girl11 from '../../images/avatar/girl-11.svg';

const Logout = (props) => {
    const avatar=girl11;
    const firebase = useContext(FirebaseContext);
    const {pseudo}=props.userData;
    const {group}=props.userData;
    return (
        <div className="logoutContainer">
           <p>{pseudo} (Group : {group})</p>
           <img className={'avatarImg'} src={avatar} alt="avatar" />
            <IconButton style={{
        marginRight: "50px",
      }} className={'tooltip'} aria-label="delete" onClick={()=>firebase.signoutUser()}>
                <ExitToAppIcon/>
                <span className={'tooltiptext'}>Quitter</span>
            </IconButton>
        </div>
    )
}

export default Logout