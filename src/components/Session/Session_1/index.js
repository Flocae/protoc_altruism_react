import {Button} from '@material-ui/core';
import {Input, Container} from '@material-ui/core';
import React, {useContext, useState} from 'react';
import {FirebaseContext} from '../../Firebase';
import ReactGoogleSlides from 'react-google-slides';
import {RespDatagrid} from '../../RespDatagrid';
import Radio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormControl from '@mui/material/FormControl';
import FormLabel from '@mui/material/FormLabel';

const Session1 = () => {
  const firebase = useContext(FirebaseContext);
  const group = firebase.userData.group;
  const [scr, setScr] = useState(0);
  const [CR, setCR] = useState(false);
  const [response, setResponse] = useState("");
  // Set Session 2 => test 2
  const getT2_test = () => {
    if (group.toString() === 'test_a') {
      return session1_T2A;
    } else if (group.toString() === 'test_b') {
      return session1_T2B;
    } else if (group.toString() === 'test_c') {
      return session1_T2C;
    }
  };

  // session
  function SwitchCase(props) {
    switch (props.value) {
      case 0:
        return Session1_1;
      case 1:
        return slide;
      case 2:
        return group.toString() === 'control' ? session1_C1 : session1_T1;
      case 3:
        return group.toString() === 'control' ? session1_C2 : getT2_test();
      case 4:
        return group.toString() === 'control' ? Session1_C3 : session1_T3;
      case 5:
        return session1_T4;
      case 6:
        return session1_T5;
      default:
        return 'Aucun scr à afficher';
    }
  }

  return (
    <div>
      <Container
        style={{
          backgroundColor: 'lightblue',
          padding: '10px',
          minHeight: '270px',
        }}
      >
        <SwitchCase value={scr} />
        {scr === 1 ? (
          <div style={{textAlign: 'center', width: '100%'}}>
            <br />
            <FormControl component="fieldset">
              <FormLabel component="legend">
                Dans cette séance nous avons parlé de :
              </FormLabel>
              <RadioGroup
                aria-label="team"
                defaultValue=""
                name="radio-buttons-group-team"
                onChange={(e) => {
                  setCR(true);
                  setResponse(e.target.value)
                }}
              >
                <FormControlLabel
                  value="altitude"
                  control={<Radio />}
                  label="Altitude"
                />
                <FormControlLabel
                  value="altruisme"
                  control={<Radio />}
                  label="Altruisme"
                />
                <FormControlLabel
                  value="alterite"
                  control={<Radio />}
                  label="Altérité"
                />
              </RadioGroup>
            </FormControl>
          </div>
        ) : (
          ''
        )}
      </Container>
      <br />

      <Button
        style={{display: 'block', margin: 'auto'}}
        disabled={scr === 1 && !CR}
        variant="contained"
        color="primary"
        onClick={() => {
          if ((group.toString() === 'control' && scr === 4) || scr === 6) {
            // end of session for controls or test if scr =6
            firebase.signoutUser();
          } 
          else {
            setScr(scr + 1);
            if (scr===1){
              let now = new Date();
              firebase.databehav(firebase.userData.pseudo).collection('session1')
              .add({
                'date':now,
                'response':response
              })
              .catch(error=>{
                console.log(error);
            })// si erreur
              /*firebase.databehav(firebase.auth.currentUser.uid).set({
                'hsd2':"nikkkkk2"
              })*/
            }
          }
        }}
      >
        Continuer
      </Button>
    </div>
  );
};

export default Session1;

// CONTENUS

const slide = (
  <ReactGoogleSlides
    width={'100%'}
    height={480}
    slidesLink="https://docs.google.com/presentation/d/1tUjhNAPXWKSHGFM2LYbHHYGjU6Fid4B7YBWB6wM1nL0"
    position={0}
    showControls
  />
);

const Session1_1 = (
  <div>
    <h3>Bienvenue pour cette première session !</h3>
    <p>
      Aujourd’hui, nous allons découvrir ensemble le mot altruisme. L’altruisme,
      qu’est-ce que c’est ? Est-ce qu’il y a d’autres mots qui veulent dire un
      peu la même chose ?
    </p>
    <p>Commençons par une présentation qui te permettra d’en savoir plus.</p>
    <p>Alors, prêts ?</p>
    <p>
      Clique sur le bouton continuer une fois que tu as bien compris pour
      continuer.
    </p>
  </div>
);
const session1_C1 = (
  <div>
    <h3>Maintenant, place au jeu !</h3>
    <p>
      Connais-tu le Petit Bac ? Les règles sont simples : nous allons te donner
      3 lettres de l’alphabet, et tu dois trouver le plus de mots possible
      commençant par ces lettres pour remplir les cases du tableau.
    </p>
  </div>
);
// TODO => remplacer RespDatagrid par input multiline plus ergonomique pour les tel et tablettes
const session1_C2 = (
  <div>
    <p>Les trois lettres pour ce jeu sont… </p>
    <h2>R, B et Y.</h2>
    <p>
      A toi de jouer ! (clique sur les cases pour pouvoir écrire tes réponses)
    </p>
    <RespDatagrid />
  </div>
);
const Session1_C3 = (
  <div>
    <h2>Bravo!</h2>
    <p>
      Nous avons terminé pour aujourd’hui. N’oublie pas de te reconnecter dans
      quelques jours pour notre prochaine séance.{' '}
    </p>
    <p>Merci pour ta participation !</p>
    <p>Clique sur le bouton ci-dessous pour quitter la séance.</p>
  </div>
);

const session1_T1 = (
  <div>
    <p>
      Tu vas maintenant pouvoir rencontrer un groupe de bénévoles comme toi.
      Vous êtes tous réunis autour de cette cause que vous avez choisie.
      Aujourd’hui, c’est la première réunion du groupe que tu vas voir à chaque
      séance.
    </p>
  </div>
);

const session1_T2A = (
  <div>
    <p>
      Bonjour à toutes et à tous ! Nous sommes ensemble ici car nous avons
      décidé de soutenir, ensemble, la cause de l’écologie. Deux fois par
      semaine, nous allons nous retrouver pour travailler ensemble sur cette
      action et avoir un impact positif sur le monde ! Merci pour ton engagement
      et pour ton aide qui va beaucoup nous servir dans notre soutien à
      l’écologie.
    </p>
    <p>On y va ? C’est parti !</p>
  </div>
);
const session1_T2B = (
  <div>
    <p>
      Bonjour à toutes et à tous ! Nous sommes ensemble ici car nous avons
      décidé d’aider, ensemble, les personnes hospitalisées. Deux fois par
      semaine, nous allons nous retrouver pour travailler ensemble sur cette
      action et avoir un impact positif sur le monde ! Merci pour ton engagement
      et pour ton aide qui va beaucoup nous servir dans notre soutien à ceux qui
      passent du temps à l’hôpital.
    </p>
    <p>On y va ? C’est parti !</p>
  </div>
);
const session1_T2C = (
  <div>
    <p>
      Bonjour à toutes et à tous ! Nous sommes ensemble ici car nous avons
      décidé d’aider, ensemble, les personnes hospitalisées. Deux fois par
      semaine, nous allons nous retrouver pour travailler ensemble sur cette
      action et avoir un impact positif sur le monde ! Merci pour ton engagement
      et pour ton aide qui va beaucoup nous servir dans notre soutien à ceux qui
      passent du temps à l’hôpital.
    </p>
    <p>On y va ? C’est parti !</p>
  </div>
);
const session1_T3 = (
  <div>
    <p>
      Dans la vidéo, tu as pu découvrir le mot altruisme. Prends bien le temps
      de te souvenir de ce que ce mot veut dire.
    </p>
    <p>
      En tant que bénévole, tu peux réfléchir à comment le mot altruisme peut
      être lié à ton action dans ce groupe et ces séances. Tu peux aussi
      réfléchir à comment l’altruisme est relié à la cause que tu as décidé de
      choisir. Pour toi, l’altruisme dans ce cas, c’est quoi ? Si ça peut
      t’aider, tu peux noter des choses sur une feuille ou un cahier.{' '}
    </p>
    <p>
      Maintenant, tu vas devoir choisir un mot qui représente l’altruisme, pour
      toi.
    </p>
    <p>Quand tu es prêt(e), clique sur « Continuer ».</p>
  </div>
);

const session1_T4 = (
  <div>
    <p>
      Si tu devais choisir un mot qui représente le mieux l’altruisme pour toi,
      lequel choisirais-tu ?{' '}
    </p>
    <p>Tu peux rentrer le mot que tu souhaites.</p>
    <Input variant="filled" type="text" id="text" autoComplete="off" required />
    <p>Quand tu as terminé, clique sur « Continuer ». </p>
  </div>
);
const session1_T5 = (
  <div>
    <h3>Bravo ! Tu as participé à la première séance de ton groupe.</h3>
    <p>
      Nous avons terminé pour aujourd’hui. N’oublie pas de te reconnecter dans
      quelques jours pour notre prochaine séance.
    </p>
    <p>Merci pour ta participation !</p>
    <p>Clique sur le bouton ci-dessous pour quitter la séance.</p>
  </div>
);

// CODE SAVE
/*<Box 
        display="flex"
        width={1/1}
        height={1/1}
        bgcolor="lightblue"
        padding={"10px"}
        justifyContent="center">

</Box>
*/
