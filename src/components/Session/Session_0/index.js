import React, {Fragment, useState} from 'react';
import {Container} from '@material-ui/core';
import DictatorGame from '../../dictatorGame';
import Shuffle from 'shuffle-array';
import {Button} from '@material-ui/core';

const Session0 = () => {
  //
  const [numScr, setNumScr] = useState(0);
  const nextScreen = () => {
    setNumScr(numScr + 1);
  };

  return (
    <div>
      <Container
        style={{
          backgroundColor: 'white',
          padding: '10px',
          minHeight: '270px',
        }}
      >
        {(numScr === 0 || numScr === 2 || numScr === 4) && (
          <div className="dictaWin">
            {numScr === 0&&<h2>Consignes</h2>}
            {numScr === 2&&<h2>Prêt ?</h2>}
            {numScr === 4&&<h2>Fini !</h2>}
            <Button
              style={{display: 'block', margin: 'auto'}}
              variant="contained"
              color="primary"
              onClick={nextScreen}
            >
              Continuer
            </Button>
          </div>
        )}
        {numScr === 1 && <DictatorGame order={[6]} nextScreen={nextScreen} title={"Exemple"}/>}
        {numScr === 3 && (
          <DictatorGame order={Shuffle([2, 3, 4, 5, 6, 7, 8, 9])} nextScreen={nextScreen} title={""} />
        )}
      </Container>
    </div>
  );
};
export default Session0;
