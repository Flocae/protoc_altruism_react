import React, {useContext} from 'react';
import {FirebaseContext} from '../Firebase';
import Session0 from './Session_0';
import Session1 from './Session_1';
import Session2 from './Session_2';

import BackgroundImg from '../../images/mainIm.jpg';

const Session = (props) => {
  const firebase = useContext(FirebaseContext);
  const currentSession = props.currentSession;
  const group = firebase.userData.group;

  // session
  function SwitchCase(props) {
    switch (props.value) {
      case 'pt':
        return <Session0 />;
      case 1:
        return <Session1 />;
      case 2:
        return <Session2 />;
      default:
        return 'Aucune Session à afficher';
    }
  }
  return (
    <div
      style={{
        backgroundImage: 'url(' + BackgroundImg + ')',
        backgroundSize: 'cover',
        padding: '20px',
        height: '800px',
      }}
    >
      <p>
        Session {currentSession} - group {group}
      </p>
      <SwitchCase value={currentSession} />
    </div>
  );
};

export default Session;
