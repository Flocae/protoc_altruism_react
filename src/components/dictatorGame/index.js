import React, {useEffect, useState, useContext} from 'react';
import {Button} from '@material-ui/core';
import {FirebaseContext} from '../Firebase';

const DictatorGame = (props) => {
  //
  const [myArray, updateMyArray] = useState([]);

  const onClick = () => {
    updateMyArray((arr) => [...arr, `${arr.length}`]);
    console.log(myArray);
  };
  //
  const firebase = useContext(FirebaseContext);
  const [currentMaxNum, setCurrentMaxNum] = useState(NaN);
  const [numEssai, setNumEssai] = useState(0);
  const [response, setResponse] = useState(NaN);
  const [responseAll, updateResponseAll] = useState([]);
  //
  useEffect(() => {
    console.log(responseAll);
    console.log(numEssai)
    setCurrentMaxNum(props.order[numEssai]);
    // Save Data
    if (numEssai === props.order.length) {
     
      if (props.order.length > 1) {
        let now = new Date();
        firebase
          .databehav(firebase.userData.pseudo)
          .collection('dictatorGame')
          .add({
            date: now,
            order: props.order,
            responses: responseAll,
          })
          .catch((error) => {
            console.log(error);
          })
          .then(props.nextScreen())
      }
      else{
        props.nextScreen();
      }
      
    }
  }, [props.order, numEssai,firebase,props,responseAll]);

  return (
    <div className="dictaWin">
      <h2>{props.title}</h2>
      <div className="dictatorContainer">
        <div
          className={response === 0 ? 'dotContainerSelect' : 'dotContainer'}
          onClick={() => setResponse(0)}
        >
          <hr className="dictatorHr" />
          <p className="dictatorNumber">0</p>
        </div>
        <div
          className={response === 1 ? 'dotContainerSelect' : 'dotContainer'}
          onClick={() => setResponse(1)}
        >
          <span className="dot"></span>
          <hr className="dictatorHr" />
          <p className="dictatorNumber">1</p>
        </div>
        {currentMaxNum > 1 && (
          <div
            className={response === 2 ? 'dotContainerSelect' : 'dotContainer'}
            onClick={() => setResponse(2)}
          >
            <span className="dot"></span>
            <span className="dot"></span>
            <hr className="dictatorHr" />
            <p className="dictatorNumber">2</p>
          </div>
        )}
        {currentMaxNum > 2 && (
          <div
            className={response === 3 ? 'dotContainerSelect' : 'dotContainer'}
            onClick={() => setResponse(3)}
          >
            <span className="dot"></span>
            <span className="dot"></span>
            <span className="dot"></span>
            <hr className="dictatorHr" />
            <p className="dictatorNumber">3</p>
          </div>
        )}
        {currentMaxNum > 3 && (
          <div
            className={response === 4 ? 'dotContainerSelect' : 'dotContainer'}
            onClick={() => setResponse(4)}
          >
            <span className="dot"></span>
            <span className="dot"></span>
            <span className="dot"></span>
            <span className="dot"></span>
            <hr className="dictatorHr" />
            <p className="dictatorNumber">4</p>
          </div>
        )}
        {currentMaxNum > 4 && (
          <div
            className={response === 5 ? 'dotContainerSelect' : 'dotContainer'}
            onClick={() => setResponse(5)}
          >
            <span className="dot"></span>
            <span className="dot"></span>
            <span className="dot"></span>
            <span className="dot"></span>
            <span className="dot"></span>
            <hr className="dictatorHr" />
            <p className="dictatorNumber">5</p>
          </div>
        )}
        {currentMaxNum > 5 && (
          <div
            className={response === 6 ? 'dotContainerSelect' : 'dotContainer'}
            onClick={() => setResponse(6)}
          >
            <span className="dot"></span>
            <span className="dot"></span>
            <span className="dot"></span>
            <span className="dot"></span>
            <span className="dot"></span>
            <span className="dot"></span>
            <hr className="dictatorHr" />
            <p className="dictatorNumber">6</p>
          </div>
        )}
        {currentMaxNum > 6 && (
          <div
            className={response === 7 ? 'dotContainerSelect' : 'dotContainer'}
            onClick={() => setResponse(7)}
          >
            <span className="dot"></span>
            <span className="dot"></span>
            <span className="dot"></span>
            <span className="dot"></span>
            <span className="dot"></span>
            <span className="dot"></span>
            <span className="dot"></span>
            <hr className="dictatorHr" />
            <p className="dictatorNumber">7</p>
          </div>
        )}
        {currentMaxNum > 7 && (
          <div
            className={response === 8 ? 'dotContainerSelect' : 'dotContainer'}
            onClick={() => setResponse(8)}
          >
            <span className="dot"></span>
            <span className="dot"></span>
            <span className="dot"></span>
            <span className="dot"></span>
            <span className="dot"></span>
            <span className="dot"></span>
            <span className="dot"></span>
            <span className="dot"></span>
            <hr className="dictatorHr" />
            <p className="dictatorNumber">8</p>
          </div>
        )}
        {currentMaxNum > 8 && (
          <div
            className={response === 9 ? 'dotContainerSelect' : 'dotContainer'}
            onClick={() => setResponse(9)}
          >
            <span className="dot"></span>
            <span className="dot"></span>
            <span className="dot"></span>
            <span className="dot"></span>
            <span className="dot"></span>
            <span className="dot"></span>
            <span className="dot"></span>
            <span className="dot"></span>
            <span className="dot"></span>
            <hr className="dictatorHr" />
            <p className="dictatorNumber">9</p>
          </div>
        )}
      </div>
      <Button
        style={{display: 'block', margin: 'auto'}}
        disabled={isNaN(response)}
        variant="contained"
        color="primary"
        onClick={() => {
          updateResponseAll((arr) => [...arr, response]);

          if (numEssai < props.order.length) {
            setResponse(NaN);
            setNumEssai(numEssai + 1);
          }
        }}
      >
        Continuer
      </Button>
      <input type="button" onClick={onClick} value="Update" />
    </div>
  );
};

export default DictatorGame;
