import React from "react";

export const LegalMention = () => {
  return (
    <div>
      <h3>Mentions légales</h3>
      <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur
        maximus diam vitae sapien lobortis, sit amet consectetur urna faucibus.
        Phasellus convallis velit a viverra congue. Ut cursus nisi a lacus
        accumsan dignissim. Pellentesque ullamcorper laoreet semper. Duis erat
        felis, interdum non ullamcorper gravida, sodales et nisi. Duis nec
        dignissim leo. Suspendisse mi dolor, tincidunt vitae ante in, pulvinar
        suscipit erat. Maecenas porta dui erat, vitae consequat ligula cursus
        blandit.
      </p>
      <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur
        maximus diam vitae sapien lobortis, sit amet consectetur urna faucibus.
        Phasellus convallis velit a viverra congue. Ut cursus nisi a lacus
        accumsan dignissim. Pellentesque ullamcorper laoreet semper. Duis erat
        felis, interdum non ullamcorper gravida, sodales et nisi. Duis nec
        dignissim leo. Suspendisse mi dolor, tincidunt vitae ante in, pulvinar
        suscipit erat. Maecenas porta dui erat, vitae consequat ligula cursus
        blandit.
      </p>
      <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur
        maximus diam vitae sapien lobortis, sit amet consectetur urna faucibus.
        Phasellus convallis velit a viverra congue. Ut cursus nisi a lacus
        accumsan dignissim. Pellentesque ullamcorper laoreet semper. Duis erat
        felis, interdum non ullamcorper gravida, sodales et nisi. Duis nec
        dignissim leo. Suspendisse mi dolor, tincidunt vitae ante in, pulvinar
        suscipit erat. Maecenas porta dui erat, vitae consequat ligula cursus
        blandit.
      </p>
    </div>
  );
};
