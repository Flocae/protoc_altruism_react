import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import '@fontsource/roboto';
import '../../App.css';
import Header from '../Header';
import Landing from '../Landing';
import Footer from '../Footer';
import Welcome from '../Welcome';
import Login from '../Login';
import Signup from '../Signup';
import ErrorPage from '../ErrorPage';
import ForgetPassword from '../ForgetPassword';
import { LegalMention } from '../legal';

function App() {
  // THIS IS THE ROOT
  return (
    <div>
      <Router>
        <Header />

        <Switch>
          <Route exact path="/" component={Landing} />
          <Route path="/welcome" component={Welcome} />
          <Route path="/login" component={Login} />
          <Route path="/signup" component={Signup} />
          <Route path="/forgetpassword" component={ForgetPassword} />
          <Route path="/legal" component={LegalMention} />
          <Route component={ErrorPage} />
        </Switch>

        <Footer />
      </Router>
    </div>
  );
}

export default App;
