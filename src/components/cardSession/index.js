import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";

const CardSession = (props) => {
  const session = props.session;
  const img = props.img;
  const text = props.text;
  const title = props.title;
  const setCurrentSession=props.setCurrentSession;
  const disabled=props.disabled;

  const useStyles = makeStyles({
    root: {
      maxWidth: 245,

    },
    media: {
      height: 240,
    
    },
  });
  const classes = useStyles();

  return (
    <div className={disabled?'sessionCardInactive':''}>
      <Card className={classes.root}>
        <CardActionArea onClick={()=>setCurrentSession(session)}>
          <CardMedia className={classes.media} image={img} title={title} />
          <CardContent>
            <Typography gutterBottom variant="h5" component="h2">
              Session {session}
            </Typography>
            <Typography variant="body2" color="textSecondary" component="p">
              {text}
            </Typography>
          </CardContent>
        </CardActionArea>
      </Card>
    </div>
  );
};

export default CardSession;
