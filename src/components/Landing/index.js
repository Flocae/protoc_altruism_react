import React from "react";
import { Link } from "react-router-dom";
import Button from "@material-ui/core/Button";

const Landing = () => {
  return (
    <main>
      <div className="welcomePage"></div>
      <div className="bg-text">
        <h2>Bonjour et bienvenue !</h2>
        <h3>Clique sur "se connecter" pour accéder à la session du jour </h3>
        
        <Button component={Link} to={"/login"} variant="contained">
          Se connecter
        </Button>
       
      </div>
      <div className="leftBox"></div>
    </main>
  );
};

export default Landing;
